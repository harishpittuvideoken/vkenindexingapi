package com.videoken.indexing;

import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.vdocipher.aegis.media.ErrorDescription;
import com.vdocipher.aegis.media.Track;
import com.vdocipher.aegis.player.VdoPlayer;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class VkenIndexing {

    private AppCompatActivity activity;
    private VdoPlayer player;
    private ToCDialogFragment toCDialogFragment;
    private FragmentTransaction fragmentTransaction;
    private String videoId;
    private String themeColor;
    FragmentManager fragmentManager;
    private String VERSION = "1.0";


    private int previousUpdatedItemIndex = -1;
    private String error;
    private ArrayList<ToCObject> tocList = new ArrayList<>();


    public VkenIndexing(AppCompatActivity activity, String videoId, String themeColor, VdoPlayer player) {
        this.activity = activity;
        this.videoId = videoId;
        this.themeColor = themeColor;
        this.player = player;
        init();
    }

    public void showToc() {
        updateTocListItem();
        showTocDailog();
    }

    public String getVersion() {
        return VERSION;
    }

    public void hideToc() {
        toCDialogFragment.dismiss();
    }

    private void showTocDailog() {
        player.setPlayWhenReady(false);
        toCDialogFragment.show(fragmentManager, "dialog");
    }

    private void init() {
        fragmentManager = this.activity.getSupportFragmentManager();
        toCDialogFragment = ToCDialogFragment.newInstance(videoId, themeColor, this.tocList);
//        toCDialogFragment.onDismiss(new DialogInterface() {
//            @Override
//            public void cancel() {
//                player.setPlayWhenReady(false);
//            }
//
//            @Override
//            public void dismiss() {
//                player.setPlayWhenReady(false);
//            }
//        });
        toCDialogFragment.setOnTocDialogListener(new ToCDialogFragment.OnTocDialogListener() {
            @Override
            public void onTocSelected(ToCObject item) {
                toCDialogFragment.dismiss();
                player.seekTo(item.getDuration() * 1000);
                player.setPlayWhenReady(true);
            }
        });

        loadToc();
    }

    private void updateTocListItem() {
        int currentTime = (int) (player.getCurrentTime() / 1000);
        if (tocList != null) {
            for (int i = 0; i < tocList.size(); i++) {
                if (currentTime <= tocList.get(i).getDuration()) {
                    if (previousUpdatedItemIndex != i) {
                        if (previousUpdatedItemIndex != -1) {
                            tocList.get(previousUpdatedItemIndex).setHighligted(false);
                        }
                        tocList.get(i).setHighligted(true);
                        previousUpdatedItemIndex = i;
                        break;
                    } else {
                        break;
                    }

                }
            }
        }
        toCDialogFragment.updateAdapter(tocList);
    }


    private void loadToc() {
        try {
            new ToCAPI(new ToCAPI.ToCAPIListener() {
                @Override
                public void onTocStarted() {
                    toCDialogFragment.onTocFetching();
//                        Toast.makeText(getContext(), "started", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onTocSuccess(ArrayList<ToCObject> toc) {
                    tocList.addAll(toc);
                    toCDialogFragment.updateAdapter(tocList);

//                        Toast.makeText(getContext(), "loaded toc", Toast.LENGTH_SHORT).show();

                    Log.d("api---", "ItemFragment: " + tocList.size());
                }

                @Override
                public void onTocFailure(String message) {
                    toCDialogFragment.onTocError(message);
//                    Toast.makeText(activity, "failuer toc -- " + message, Toast.LENGTH_SHORT).show();

                }
            }).execute(this.videoId).get();

        } catch (ExecutionException e) {
            Toast.makeText(this.activity, e.getMessage(), Toast.LENGTH_SHORT).show();
            this.error = e.getMessage();
            e.printStackTrace();
        } catch (InterruptedException e) {
            Toast.makeText(this.activity, e.getMessage(), Toast.LENGTH_SHORT).show();
            this.error = e.getMessage();
            e.printStackTrace();
        }
    }


}
