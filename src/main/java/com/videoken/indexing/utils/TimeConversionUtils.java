package com.videoken.indexing.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by harishpittu on 31/08/17.
 */

public class TimeConversionUtils {

    /**
     * format duration form milliseconds to readable format 00:00:00 / 00:00:00
     *
     * @param currentDuration long
     * @param totalDuration   long
     * @return string
     */
    public static String getFormattedDuration(long currentDuration, long totalDuration) {
        return String.format(Locale.getDefault(), "%02d:%02d:%02d/%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(currentDuration), TimeUnit.MILLISECONDS.toMinutes(currentDuration), TimeUnit.MILLISECONDS.toSeconds(currentDuration), TimeUnit.MILLISECONDS.toHours(totalDuration), TimeUnit.MILLISECONDS.toMinutes(totalDuration), TimeUnit.MILLISECONDS.toSeconds(totalDuration)
        );
    }


    /**
     * format duration form milliseconds to readable format
     *
     * @param duration long
     * @return string
     */
    public static String getFormattedDurationMills(long duration) {

        int[] splittedTime = getSplittedTime(duration);
        return String.format(Locale.getDefault(), "%02d:%02d:%02d",
                splittedTime[0], splittedTime[1], splittedTime[2]
        );
    }

    public static String getFormattedUtcTime(String time){
//        "dd MMM, yyyy HH:mm"
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        try {
            Date date = sdf.parse(time);
            SimpleDateFormat parseFormat = new SimpleDateFormat("dd MMM, yyyy HH:mm", Locale.getDefault());
           return parseFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "no time";
        }
    }

    /**
     * format duration form seconds to readable format
     *
     * @param duration integer
     * @return string
     */
    public static String getFormattedDuration(Integer duration) {

//        int[] splittedTime = getSplittedTime(duration);
//        return String.format(Locale.getDefault(), "%02d:%02d:%02d",
//                splittedTime[0], splittedTime[1], splittedTime[2]
//        );

        int[] splittedTime = getSplittedTime(duration);

        String finalFormattedDuration = "";

        if (splittedTime[0] != 0) {

            finalFormattedDuration +=   String.format(Locale.getDefault(), "%02d",splittedTime[0]) + ":";
        }

//        if (splittedTime[1] != 0) {
//            finalFormattedDuration +=   String.format(Locale.getDefault(), "%02d",splittedTime[1]) + ":";
//        }else{
//            finalFormattedDuration +=   String.format(Locale.getDefault(), "%02d",0 + ":";
//
//        }
        finalFormattedDuration +=   String.format(Locale.getDefault(), "%02d",splittedTime[1]) + ":";

        finalFormattedDuration +=  String.format(Locale.getDefault(), "%02d",splittedTime[2]) ;
        return finalFormattedDuration;
    }


    /**
     * format duration form seconds to readable format
     *
     * @param duration integer
     * @return string
     */
    public static String getFormattedDurationHumanReadable(Integer duration) {

        int[] splittedTime = getSplittedTime(duration);
        String finalFormattedDuration = "";
        if (splittedTime[0] != 0) {
            finalFormattedDuration += splittedTime[0] + "h ";
        }

        if (splittedTime[1] != 0) {
            finalFormattedDuration += splittedTime[1] + "m ";
        }

//        if (splittedTime[2] != 0) {
            finalFormattedDuration += splittedTime[2] + "s ";
//        }
        return finalFormattedDuration;
    }

    /**
     * format duration form seconds to readable format
     *
     * @param duration String in hh:mm:ss format
     * @return string
     */
    public static String getFormattedDurationHumanReadable(String duration) {

        if(TextUtils.isEmpty(duration)){
            return "00h 00m 00s";
        }else {
            String[] time = duration.split(":");
            String finalFormattedDuration = "";
            if (Integer.parseInt(time[0]) != 0) {
                finalFormattedDuration += Integer.parseInt(time[0]) + "h ";
            }

            if (Integer.parseInt(time[1]) != 0) {
                finalFormattedDuration += Integer.parseInt(time[1]) + "m ";
            }

//        if (splittedTime[2] != 0) {
            finalFormattedDuration += Integer.parseInt(time[2]) + "s ";
//        }
            return finalFormattedDuration;
        }
    }

    /**
     * format duration form milliseconds to readable format 00:00:00 TO 00:00:00
     *
     * @param fromduration long
     * @param toduration   long
     * @return string
     */
    public static String getFormattedStripDuration(int fromduration, int toduration) {
        int[] fromSplittedTime = getSplittedTime(fromduration);
        int[] toSplittedTime = getSplittedTime(toduration);

        return String.format(Locale.getDefault(), "%02d:%02d:%02d TO %02d:%02d:%02d",
                fromSplittedTime[0], fromSplittedTime[1], fromSplittedTime[2], toSplittedTime[0], toSplittedTime[1], toSplittedTime[2]
        );
    }

    /**
     * get splittime of h,m ,s from millliseconds
     */
    private static int[] getSplittedTime(long milliseconds) {
        int totalSecs = (int) TimeUnit.MILLISECONDS.toSeconds(milliseconds);
        return getSplittedTime(totalSecs);
    }


    /**
     * to get seconds
     *
     * @param milliseconds long
     * @return int
     */
    public static int getSeconds(long milliseconds) {
        return (int) TimeUnit.MILLISECONDS.toSeconds(milliseconds);
    }


    /**
     * to get milliseconds
     *
     * @param seconds long
     * @return int
     */
    public static int getMilliSeconds(Integer seconds) {
        return (int) TimeUnit.SECONDS.toMillis(seconds);
    }

    /**
     * get splittime of h,m ,s from seconds
     */
    private static int[] getSplittedTime(int totalSecs) {
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        int[] splitTime = new int[3];
        splitTime[0] = hours;
        splitTime[1] = minutes;
        splitTime[2] = seconds;
        return splitTime;
    }



    public static String doConvertDateToddMMMyyyy(String time) {
        try {
            time = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }
}
