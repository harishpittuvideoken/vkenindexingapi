package com.videoken.indexing;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.videoken.indexing.utils.TimeConversionUtils;

import java.util.List;

public class ToCRecyclerViewAdapter extends RecyclerView.Adapter<ToCRecyclerViewAdapter.ViewHolder> {

    private final List<ToCObject> toCObjects;
    private String theme;
    private final ToCDialogFragment.OnTocDialogListener onTocDialogListener;

    public ToCRecyclerViewAdapter(List<ToCObject> items,String theme, ToCDialogFragment.OnTocDialogListener onTocDialogListener) {
        toCObjects = items;
        this.onTocDialogListener = onTocDialogListener;
        this.theme = theme;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        if(viewType == R.layout.toc_item_highlight){
            Drawable background = view.getBackground();

            if (background instanceof ShapeDrawable) {
                ((ShapeDrawable) background.mutate()).getPaint().setColor((Color.parseColor(theme)));
            } else if (background instanceof GradientDrawable) {
                ((GradientDrawable) background.mutate()).setColor((Color.parseColor(theme)));
            } else if (background instanceof ColorDrawable) {
                ((ColorDrawable) background.mutate()).setColor((Color.parseColor(theme)));
            }else{
                Log.w("color---","Not a valid background type");
            }
        }
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return toCObjects.get(position).isHighligted() ? R.layout.toc_item_highlight : R.layout.toc_item;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ToCObject tocObject = toCObjects.get(position);
        holder.durationTextView.setText(TimeConversionUtils.getFormattedDuration(tocObject.getDuration()));
        holder.titleTextView.setText(tocObject.getTitle());
        String index = (position + 1) + ".";
        holder.indexTextView.setText(index);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onTocDialogListener) {
                    onTocDialogListener.onTocSelected(tocObject);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return toCObjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView indexTextView;
        final TextView titleTextView;
        final TextView durationTextView;
        public ToCObject mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            indexTextView = (TextView) view.findViewById(R.id.index);
            titleTextView = (TextView) view.findViewById(R.id.title);
            durationTextView = (TextView) view.findViewById(R.id.duration);

        }

    }
}
