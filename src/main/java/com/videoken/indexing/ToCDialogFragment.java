package com.videoken.indexing;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class ToCDialogFragment extends DialogFragment {
    private OnTocDialogListener onTocDialogListener;
    private RecyclerView recyclerView;
    private TextView errorTextVeiw;
    private RelativeLayout progressBar;
    private ToCRecyclerViewAdapter recyclerViewAdapter;
    private View view;
    private ArrayList<ToCObject> toCObjects = new ArrayList<>();
    private String errorMessage;
    private String theme;

    public static ToCDialogFragment newInstance(String videoId, String themeColor, ArrayList<ToCObject> toCObjects) {
        ToCDialogFragment toCDialogFragment = new ToCDialogFragment();
        toCDialogFragment.setToCObjects(toCObjects);

        if (themeColor == null) {
            themeColor = "#03A9F4";
        }
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("videoId", videoId);
        args.putString("themeColor", themeColor);
        toCDialogFragment.setArguments(args);

        return toCDialogFragment;
    }

    public void setToCObjects(ArrayList<ToCObject> toCObjects) {
        this.toCObjects = toCObjects;
    }

    public void updateAdapter(ArrayList<ToCObject> toCObjects) {

        if (recyclerViewAdapter != null) {
            this.toCObjects = toCObjects;
            recyclerViewAdapter.notifyDataSetChanged();
            recyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            errorTextVeiw.setVisibility(View.GONE);
        }

    }

    public void setOnTocDialogListener(OnTocDialogListener onTocDialogListener) {
        this.onTocDialogListener = onTocDialogListener;
    }

    public void onTocError(String message) {
        if (recyclerView != null) {
            recyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            errorTextVeiw.setVisibility(View.VISIBLE);
            errorTextVeiw.setText(message);
        }
        this.errorMessage = message;
    }

    public void onTocFetching() {
//        recyclerView.setVisibility(View.GONE);
//        progressBar.setVisibility(View.VISIBLE);
//        errorTextVeiw.setVisibility(View.GONE);;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            theme = bundle.getString("themeColor", "#03A9F4");
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.toc_dialog_fragment, container, false);
        setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Set the adapter
        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.toc_list);
        RelativeLayout tocHeading = view.findViewById(R.id.toc_heading);
        Drawable background = tocHeading.getBackground();

        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background.mutate()).getPaint().setColor((Color.parseColor(theme)));
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background.mutate()).setColor((Color.parseColor(theme)));
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background.mutate()).setColor((Color.parseColor(theme)));
        } else {
            Log.w("color---", "Not a valid background type");
        }

        errorTextVeiw = view.findViewById(R.id.error_textview);
        progressBar = view.findViewById(R.id.progress);
        ImageView closeButton = view.findViewById(R.id.close);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerViewAdapter = new ToCRecyclerViewAdapter(toCObjects, theme, onTocDialogListener);
        recyclerView.setAdapter(recyclerViewAdapter);

        if (errorMessage != null) {
            recyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            errorTextVeiw.setVisibility(View.VISIBLE);
            errorTextVeiw.setText(this.errorMessage);
        } else if (this.toCObjects.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            errorTextVeiw.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            errorTextVeiw.setVisibility(View.GONE);
        }

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }


    public interface OnTocDialogListener {
        void onTocSelected(ToCObject item);
    }
}